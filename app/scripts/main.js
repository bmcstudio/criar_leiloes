(function(window, history, undefined) {

	"use strict";

    var location = window.history.location || window.location;

    // Bind to StateChange Event
    // History.Adapter.bind(window, 'statechange', function() { // Note: We are using statechange instead of popstate
    window.addEventListener("popstate", function() { // Note: We are using statechange instead of popstate
        // changePage(location.pathname); // Note: We are using History.getState() instead of event.state

        if (location.pathname.split("/").pop() != '') {
            var prevUrl = 'pages/'+location.pathname.split("/").pop();
            changePage(prevUrl);
        } else {
            changePage(location.pathname  + 'pages/home.php');
        }
    });

    document.addEventListener('DOMContentLoaded', function () {
    	changePage(location.pathname + 'pages/home.php');
    });

    var links = document.querySelectorAll('.link');
    // [].slice.call( links ).forEach( function( bttn ) {
    Array.prototype.forEach.call(links, function(elem, id) {
        elem.addEventListener('click', function(e) {
            changePage(elem.href);
            history.pushState(null, null, elem.href.split("/").pop());
            e.preventDefault();
        }, false);
    });

    function addHandleLink() {
        window.scrollTo(0, 0);

        var insideLinks = document.querySelectorAll('#main .link');
        Array.prototype.forEach.call(insideLinks, function(elem, id) {
            elem.addEventListener('click', function(e) {
                changePage(elem.href);
                history.pushState(null, null, elem.href.split("/").pop());
                e.preventDefault();
            }, false);
        });
    }

    function changePage(href) {

        $.ajax({url: href}).done(successCallback).fail(errorCallback);

        function successCallback (data) {
            $('#main').html(data);
            addHandleLink();
        }

        function errorCallback (data) {
            console.log('deu erro dos brabo!');
        }
    }

})(window, window.history);


// // $(function(){

// //     var textfield = $("input[name=user]");

// //     $('button[type="submit"]').click(function(e) {
// //         e.preventDefault();
// //         //little validation just to check username
// //         if (textfield.val() != "") {

// //             //$("body").scrollTo("#output");
// //             $("#output").addClass("alert alert-success animated fadeInUp").html("Welcome back " + "<span style='text-transform:uppercase'>" + textfield.val() + "</span>");
// //             $("#output").removeClass(' alert-danger');
// //             $("input").css({
// //             "height":"0",
// //             "padding":"0",
// //             "margin":"0",
// //             "opacity":"0"
// //             });

// //             //change button text
// //             $('button[type="submit"]').html("continue")
// //                 .removeClass("btn-info")
// //                 .addClass("btn-default").click(function(){
// //                     console.log('aqui');
// //                 $("input").css({
// //                     "height":"auto",
// //                     "padding":"10px",
// //                     "opacity":"1"
// //                     }).val("");
// //                 });

// //             //show avatar
// //             $(".avatar").css({
// //                 "background-image": "url('http://api.randomuser.me/0.3.2/portraits/women/35.jpg')"
// //             });

// //         } else {
// //             //remove success mesage replaced with error message
// //             $("#output").removeClass(' alert alert-success');
// //             $("#output").addClass("alert alert-danger animated fadeInUp").html("sorry enter a username ");
// //         }
// //         //console.log(textfield.val());

// //     });
// // });