<div class="page-header">
  <h1>Fale conosco</h1>
</div>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam explicabo ea qui dolore veritatis expedita reprehenderit. Possimus, rerum quis iure!</p>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam explicabo ea qui dolore veritatis expedita reprehenderit. Possimus, rerum quis iure!</p>
<form>
    <div class="form-group">
        <label for="inputNome">Nome:</label>
        <input type="text" class="form-control" id="inputNome" placeholder="" required>
    </div>
    <div class="form-group">
        <label for="inputEmail">Email:</label>
        <input type="email" class="form-control" id="inputEmail" placeholder="" required>
    </div>
    <div class="form-group">
        <label for="inputMsg">Mensagem</label>
        <textarea class="form-control" id="inputMsg" rows="5" required required data-errormessage-value-missing="Este campo é obrigatório"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Evniar</button>
</form>