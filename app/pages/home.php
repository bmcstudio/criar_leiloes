<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="images/slider/slide_1.jpg" alt="...">
            <div class="carousel-caption">
                Lorem ipsum dolor sit amet.
            </div>
        </div>
        <div class="item">
            <img src="images/slider/slide_1.jpg" alt="...">
            <div class="carousel-caption">
                Lorem ipsum dolor sit amet.
            </div>
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
    </a>
</div>
<div class="row content-agenda">
    <h1>Agenda</h1>
    <div class="col-lg-6">
        <div class="box-agenda">
            <div class="flex-img">
                <img src="images/agenda/leilao1.jpg" height="277" width="196" alt="">
            </div>
            <div class="flex-txt">
                <h3>LEILÃO RANCHO VENEZZA & CONVIDADOS</h3>
                <p>Data: Dia 28 de Março, 2015, 20 horas.</p>
                <p>Local: Vitória da Conquista, BA</p>
                <p>Asessoria: Hippus - (31) 9105.1100</p>
                <p>Transmissão: Novo Canal Obs.: Durante a Exposição de Vitória da Conquista.</p>
            </div>
            <a href="pages/leilao.php" class="link btn-saibamais">saiba mais</a>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box-agenda">
            <div class="flex-img">
                <img src="images/agenda/leilao1.jpg" height="277" width="196" alt="">
            </div>
            <div class="flex-txt">
                <h3>LEILÃO RANCHO VENEZZA & CONVIDADOS</h3>
                <p>Data: Dia 28 de Março, 2015, 20 horas.</p>
                <p>Local: Vitória da Conquista, BA</p>
                <p>Asessoria: Hippus - (31) 9105.1100</p>
                <p>Transmissão: Novo Canal Obs.: Durante a Exposição de Vitória da Conquista.</p>
            </div>
            <a href="pages/leilao.php" class="btn-saibamais">saiba mais</a>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box-agenda">
            <div class="flex-img">
                <img src="images/agenda/leilao1.jpg" height="277" width="196" alt="">
            </div>
            <div class="flex-txt">
                <h3>LEILÃO RANCHO VENEZZA & CONVIDADOS</h3>
                <p>Data: Dia 28 de Março, 2015, 20 horas.</p>
                <p>Local: Vitória da Conquista, BA</p>
                <p>Asessoria: Hippus - (31) 9105.1100</p>
                <p>Transmissão: Novo Canal Obs.: Durante a Exposição de Vitória da Conquista.</p>
            </div>
            <a href="pages/leilao.php" class="btn-saibamais">saiba mais</a>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box-agenda">
            <div class="flex-img">
                <img src="images/agenda/leilao1.jpg" height="277" width="196" alt="">
            </div>
            <div class="flex-txt">
                <h3>LEILÃO RANCHO VENEZZA & CONVIDADOS</h3>
                <p>Data: Dia 28 de Março, 2015, 20 horas.</p>
                <p>Local: Vitória da Conquista, BA</p>
                <p>Asessoria: Hippus - (31) 9105.1100</p>
                <p>Transmissão: Novo Canal Obs.: Durante a Exposição de Vitória da Conquista.</p>
            </div>
            <a href="pages/leilao.php" class="btn-saibamais">saiba mais</a>
        </div>
    </div>
</div>