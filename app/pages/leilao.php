<div class="page-header">
  <h1>Leilão online</h1>
</div>

<div class="header-leilao">

    <div class="row">
        <div class="col-md-3 col-lg-3 text-center">
            <img src="images/agenda/leilao1.jpg" height="277" width="196" alt="">
            <div class="mg-sm countdown"></div>
        </div>

        <div class=" col-md-9 col-lg-9 ">
            <table class="table table-responsive table-information">
                <tbody>
                    <tr>
                        <td>Data de término:</td>
                        <td>25/02/2015 às 22:00:00</td>
                    </tr>
                    <tr>
                        <td>Transmissão:</td>
                        <td>online - Somente pela internet</td>
                    </tr>
                    <tr>
                        <td>Leiloeiro:</td>
                        <td>Lorem ipsum dolor</td>
                    </tr>
                    <tr>
                        <td>Assessoria:</td>
                        <td>Lorem ipsum</td>
                    </tr>
                    <tr>
                        <td>Comissão:</td>
                        <td>5%</td>
                    </tr>
                    <tr>
                        <td>Condição de pagamento:</td>
                        <td>30 parcelas 2+2+2+24</td>
                    </tr>
                    <tr>
                        <td>Observações:</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat soluta ad, quisquam iusto distinctio aut ipsum totam cupiditate repudiandae unde.</td>
                    </tr>
                </tbody>
            </table>
            <ul class="list-social">
                <li><a href="#" target="_blank" class="icon-social icon-facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank" class="icon-social icon-twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" target="_blank" class="icon-social icon-google-plus"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="winner">
                        <img src="images/hand_win.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                	<span class="txt-price">US$3000,00</span>
                	<a href="#" class="btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="looser">
                        <img src="images/hand_lose.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                	<span class="txt-price">US$3000,00</span>
                	<a href="#" class="btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="winner">
                        <img src="images/hand_win.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                	<span class="txt-price">US$3000,00</span>
                	<a href="#" class="btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="winner">
                        <img src="images/hand_win.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                    <span class="txt-price">US$3000,00</span>
                    <a href="#" class="link btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="winner">
                        <img src="images/hand_win.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                    <span class="txt-price">US$3000,00</span>
                    <a href="#" class="btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="looser">
                        <img src="images/hand_lose.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                    <span class="txt-price">US$3000,00</span>
                    <a href="#" class="btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="looser">
                        <img src="images/hand_lose.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                    <span class="txt-price">US$3000,00</span>
                    <a href="#" class="link btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="winner">
                        <img src="images/hand_win.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                    <span class="txt-price">US$3000,00</span>
                    <a href="#" class="btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <a href="pages/info-cavalo.php" class="relative overlay-horse link">
                <img src="http://placehold.it/300x300" alt="...">
                <div class="view">
                    <img src="images/icon-view-more.svg" alt="" height="64" width="64">
                </div>
                <div class="current-status">
                    <div class="looser">
                        <img src="images/hand_lose.svg" height="48" width="48" alt="vencendo">
                    </div>
                </div>
            </a>
            <div class="caption relative">
                <h3>Wimpyneedsacocktail</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, neque?</p>
                <p class="pd-top-sm">
                    <span class="txt-price">US$3000,00</span>
                    <a href="#" class="btn-saibamais">dar lance</a>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="leilao-float">
    <div class="relative">
        <div class="text-center">
            <div class="mg-sm countdown"></div>
        </div>
        <button id="btnFloat" class="btn-float btn-float-right">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        </button>
    </div>
</div>

<script>
    var austDay = new Date(2015, 1 - 1, 15, 22, 00, 00);
    $('.countdown').countdown({
      until: austDay,
      compact: true,
      description: ' Para finalizar o leilão!'
    });

    // show/hide leilao-float

    // variables
    var float_leilao = document.querySelector('.leilao-float'),
        btnFloat     = document.querySelector('#btnFloat'),
        icon         = document.querySelector('#btnFloat .glyphicon');

    // events
    window.onscroll  = spyScroll;
    btnFloat.onclick = handleFloat;

    function spyScroll(e) {

        if (window.pageYOffset >= 700) {
            if (!float_leilao.classList.contains('hide-float'))
                float_leilao.classList.add('show-float');
        }
        else{
            float_leilao.classList.remove('show-float');
            if (float_leilao.classList.contains('hide-float')){
                toggleButton();
                float_leilao.classList.remove('hide-float');
            }
        }
    }

    function handleFloat() {
        float_leilao.classList.toggle('show-float');
        float_leilao.classList.toggle('hide-float');

        toggleButton();
    }

    function toggleButton () {
        btnFloat.classList.toggle('btn-float-right');
        btnFloat.classList.toggle('btn-float-left');

        icon.classList.toggle('glyphicon-chevron-right');
        icon.classList.toggle('glyphicon-chevron-left');
    }
</script>