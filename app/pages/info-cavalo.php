<div class="resume">
    <header class="page-header text-center">
        <h1 class="page-title">Wimpyneedsacocktail</h1>
    </header>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
            <div class="panel panel-default">
                <div class="panel-heading resume-heading">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-xs-12 col-sm-4">
                                <figure>
                                    <img class="img-thumbnail img-responsive" alt="" src="images/img-teste.jpg">
                                </figure>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <div class="relative">
                                    <div class="relative box-value-leilao">
                                        <p>Valor da parcela</p>
                                        <span class="txt-price-leilao">R$400,00</span>
                                    </div>
                                    <div class="text-center">
                                        <div class="mg-sm">
                                            <img src="images/hand_win.svg" height="48" width="48" alt="vencendo">
                                        </div>
                                        <div class="mg-sm countdown"></div>
                                        <div class="mg-sm mg-top-sm">
                                            <button class="btn btn-success">dar lance</button>
                                        </div>
                                    </div>
                                    <ul class="list-group list-info-leilao">
                                        <li class="list-group-item"> valor de cada lance R$ 20,00 </li>
                                        <li class="list-group-item"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, molestias. </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <div class="well well-sm">
                        <div class="btn-group">
                            <a href="#" id="tab-image" class="btn btn-default"><span class="glyphicon glyphicon-picture"></span>Imagens</a>
                            <a href="#" id="tab-video" class="btn btn-default"><span class="glyphicon glyphicon-film"></span>Vídeos</a>
                        </div>
                    </div>
                    <!-- image slider -->
	                <div id="image-slider" class="slider-show carousel carousel-resume slide article-slide">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner cont-slider">

                            <div class="item active">
                                <img alt="" title="" src="images/slider/cavalo1.jpg">
                            </div>
                            <div class="item">
                                <img alt="" title="" src="images/slider/cavalo2.jpg">
                            </div>
                            <div class="item">
                                <img alt="" title="" src="images/slider/cavalo3.jpg">
                            </div>
                            <div class="item">
                                <img alt="" title="" src="images/slider/cavalo4.jpg">
                            </div>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li class="active" data-slide-to="0" data-target="#image-slider">
                                <img alt="" src="images/slider/cavalo1.jpg">
                            </li>
                            <li class="" data-slide-to="1" data-target="#image-slider">
                                <img alt="" src="images/slider/cavalo2.jpg">
                            </li>
                            <li class="" data-slide-to="2" data-target="#image-slider">
                                <img alt="" src="images/slider/cavalo3.jpg">
                            </li>
                            <li class="" data-slide-to="3" data-target="#image-slider">
                                <img alt="" src="images/slider/cavalo4.jpg">
                            </li>
                        </ol>
                    </div>

                    <!-- video slider -->
                    <div id="video-slider" class="slider-hide carousel carousel-resume slide article-slide">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner cont-slider">

                            <div class="item active">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="100%" class="embed-responsive-item"  src="//www.youtube.com/embed/-2fBQZFFcVg" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="item">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="100%" class="embed-responsive-item"  src="//www.youtube.com/embed/-2fBQZFFcVg" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="item">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="100%" class="embed-responsive-item"  src="//www.youtube.com/embed/-2fBQZFFcVg" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="item">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe width="100%" height="100%" class="embed-responsive-item"  src="//www.youtube.com/embed/-2fBQZFFcVg" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li class="active" data-slide-to="0" data-target="#video-slider">
                                <img alt="" src="images/slider/cavalo1.jpg">
                            </li>
                            <li class="" data-slide-to="1" data-target="#video-slider">
                                <img alt="" src="images/slider/cavalo1.jpg">
                            </li>
                            <li class="" data-slide-to="2" data-target="#video-slider">
                                <img alt="" src="images/slider/cavalo1.jpg">
                            </li>
                            <li class="" data-slide-to="3" data-target="#video-slider">
                                <img alt="" src="images/slider/cavalo1.jpg">
                            </li>
                        </ol>
                    </div>
				</div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Detalhes</h4>
                    <ul class="list-group">
                        <li class="list-group-item"> Pai: WIMPYS LITTLE STEP </li>
                        <li class="list-group-item"> Mãe: SEVEN S MIMOSA </li>
                        <li class="list-group-item"> Sexo: Macho </li>
                        <li class="list-group-item"> Raça: QM </li>
                        <li class="list-group-item"> Pelagem: BUCKSKIN </li>
                        <li class="list-group-item"> Data de nascimento: 15/03/2007 </li>
                        <li class="list-group-item"> Local onde se encontra: XTRA QH USA </li>
                    </ul>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Comentários</h4>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt perspiciatis facilis dolore corporis voluptatum officia veritatis rerum libero reiciendis harum.</p>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Campanhas</h4>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt perspiciatis facilis dolore corporis voluptatum officia veritatis rerum libero reiciendis harum.</p>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt perspiciatis facilis dolore corporis voluptatum officia veritatis rerum libero reiciendis harum.</p>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt perspiciatis facilis dolore corporis voluptatum officia veritatis rerum libero reiciendis harum.</p>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt perspiciatis facilis dolore corporis voluptatum officia veritatis rerum libero reiciendis harum.</p>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt perspiciatis facilis dolore corporis voluptatum officia veritatis rerum libero reiciendis harum.</p>
                </div>
                <div class="bs-callout bs-callout-danger">
                    <h4>Histórico veterinário</h4>
                    <table class="table table-striped table-responsive ">
                        <tbody>
                            <tr>
                                <td>Prognata</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>Aerofagico</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>Criptorquídico</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>Cirurgia de Neurectomia</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>Já teve Laminite</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>DPOC</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>Cirurgia de Cólica</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>Cicatrizes</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>HYPP</td>
                                <td>não</td>
                            </tr>
                            <tr>
                                <td>Defeito</td>
                                <td>NC</td>
                            </tr>
                            <tr>
                                <td>Cirurgia Grave</td>
                                <td>não</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="bs-callout bs-callout-danger">
                	<h4>Pedigree</h4>
                	<div id="arvore_origem">
						<ul class="pais">
							<li>
								<div>WIMPYS LITTLE STEP</div>
								<ul class="avos">
									<li>
										<div>NU CHEX TO CASH</div>
										<ul class="bisavos">
											<li>
												<div>NU CASH</div>
											</li>
											<li>
												<div>AMARILA CHEX</div>
											</li>
										</ul>
									</li>
									<li>
										<div>LEOLITA STEP</div>
										<ul class="bisavos">
											<li>
												<div>FORTY SEVEN</div>
											</li>
											<li>
												<div>STEP LITE</div>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<div>SEVEN S MIMOSA</div>
								<ul class="avos">
									<li>
										<div>HOLLYWOOD DUNIT</div>
										<ul class="bisavos">
											<li>
												<div>HOLLYWOOD JAC 86</div>
											</li>
											<li>
												<div>BLOSSOM BERY</div>
											</li>
										</ul>
									</li>
									<li>
										<div>MAY DAY HOBBY</div>
										<ul class="bisavos">
											<li>
												<div>HOBBY HORSE</div>
											</li>
											<li>
												<div>ROYAL IDA MAY</div>
											</li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
					</div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="leilao-float">
    <div class="relative">
        <div class="box-value-leilao-float">
            <p>valor da parcela</p>
            <span class="txt-price-leilao-float">R$400,00</span>
        </div>
        <div class="text-center">
            <div class="mg-sm">
                <img src="images/hand_win.svg" height="48" width="48" alt="vencendo">
            </div>
            <div class="mg-sm countdown"></div>
            <div class="mg-sm mg-top-sm">
                <button class="btn btn-success">dar lance</button>
            </div>
        </div>
        <button id="btnFloat" class="btn-float btn-float-right">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        </button>
    </div>
</div>

<script>

    // Stop carousel
    $('.carousel').carousel({
        interval: false
    });

    // tab events
    var img_slider = $('#image-slider'),
        vid_slider = $('#video-slider');

    $('#tab-image').click( toggleImageSlider );
    $('#tab-video').click( toggleVideoSlider );

    function toggleImageSlider(e) {
        e.preventDefault();

        img_slider.addClass('slider-show');
        img_slider.removeClass('slider-hide');

        vid_slider.addClass('slider-hide');
        vid_slider.removeClass('slider-show');
    }
    function toggleVideoSlider(e) {
        e.preventDefault();

        img_slider.addClass('slider-hide');
        img_slider.removeClass('slider-show');

        vid_slider.addClass('slider-show');
        vid_slider.removeClass('slider-hide');
    }

	var austDay = new Date(2015, 1 - 1, 15, 22, 00, 00);
    $('.countdown').countdown({
      until: austDay,
      compact: true,
      description: ' Para finalizar o leilão!'
    });

    // show/hide leilao-float

    // variables
    var float_leilao = document.querySelector('.leilao-float'),
        btnFloat     = document.querySelector('#btnFloat'),
        icon         = document.querySelector('#btnFloat .glyphicon');

    // events
    window.onscroll  = spyScroll;
    btnFloat.onclick = handleFloat;

    function spyScroll(e) {

        if (window.pageYOffset >= 700) {
            if (!float_leilao.classList.contains('hide-float'))
                float_leilao.classList.add('show-float');
        }
        else{
            float_leilao.classList.remove('show-float');
            if (float_leilao.classList.contains('hide-float')){
                toggleButton();
                float_leilao.classList.remove('hide-float');
            }
        }
    }

    function handleFloat() {
        float_leilao.classList.toggle('show-float');
        float_leilao.classList.toggle('hide-float');

        toggleButton();
    }

    function toggleButton () {
        btnFloat.classList.toggle('btn-float-right');
        btnFloat.classList.toggle('btn-float-left');

        icon.classList.toggle('glyphicon-chevron-right');
        icon.classList.toggle('glyphicon-chevron-left');
    }
</script>